package lukas.chessgame;

import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class SpielfeldTest {

    @Test
    public void testAdd1(){
       assertEquals(Spielfeld.add(5, 5), 10);
    }

    @Test
    // This test is supposed to fail
    public void testAddFail(){
        assertEquals(Spielfeld.add(5, 5), 11);
    }


}
