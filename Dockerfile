FROM openjdk:11-oraclelinux7
MAINTAINER: Lukas Biernat

COPY chessgame-1.0-SNAPSHOT.jar opt/application/chessgame-1.0-SNAPSHOT.jar
WORKDIR /opt/application

CMD ["java","-jar","/opt/application/chessgame-1.0-SNAPSHOT.jar"]